export interface TicketChange {
 price?: number,
 description?: string,
 image?: string,
 status?: string
}

export interface SortParams {
  dir?: "ASC" | "DESC",
  sort?: "user" | "price" | "post_date",
  start?: number
}
 
