import { JsonController, Get, Post, HttpCode, Body, BadRequestError, QueryParam, CurrentUser, UnauthorizedError } from 'routing-controllers'
import Happening from './entity'
import { getRepository } from 'typeorm';
import * as moment from 'moment';
import User from '../users/entity';

@JsonController()
export default class HappeningsController {
  @Get('/happenings')
  async getHappenings(
    @QueryParam('start') start : number
  ) {
    start = start || 0
    const happenings = await getRepository(Happening)
    .createQueryBuilder('happening')
    .where('end_date >= :now', {now: moment().toISOString()})
    .orderBy('start_date', 'DESC')
    .offset(start)
    .limit(9)
    .getManyAndCount()

    return {
      happenings: happenings[0],
      hasMore: happenings[1] > (happenings[0].length + start)
    }
  }

  @Post('/happenings')
  async addHappening(
    @HttpCode(201)
    @CurrentUser() user: User,
    @Body() happening: any
  ) {
    let finalHappening = new Happening()

    //TODO: proper error handling

    if(!happening.name || happening.name === null) throw BadRequestError
    if(!happening.description || happening.description === null) throw BadRequestError
    if(!happening.startDate || happening.startDate === null) throw BadRequestError
    
    if(!user) throw new UnauthorizedError
    
    if(!happening.endDate || happening.endDate === null) {
      happening.endDate = new Date(happening.startDate)
    }
    happening.startDate = new Date(happening.startDate)
    finalHappening = happening
    return await Happening.save(finalHappening)
  }
}