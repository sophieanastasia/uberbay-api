import { 
  JsonController, 
  CurrentUser, 
  Get, 
  Post, 
  HttpCode, 
  Body, 
  BadRequestError, 
  Param, 
  Authorized, 
  Patch, 
  UnauthorizedError,
  Delete,
  QueryParams,
  NotFoundError
} from 'routing-controllers'

import Ticket from './entity'
import User from '../users/entity';
import { TicketChange, SortParams } from './validation';
import { getRiskScore } from './fraudScore'
import { getRepository } from '../../node_modules/typeorm';

@JsonController()
export default class TicketsController {
  @Get('/tickets')
  async getTickets(
    @QueryParams() params :SortParams
  ) {
    //defaultValues
    params.start = params.start || 0,
    params.sort = params.sort || 'post_date'
    params.dir = params.dir || "ASC"

    const tickets = await getRepository(Ticket)
      .createQueryBuilder('ticket')
      .orderBy(params.sort, params.dir)
      .offset(params.start)
      .limit(9)
      .getMany()
    return tickets
  }

  @Get('/tickets/:id')
  async getTicketById(
    @Param('id') id: number,
  ) {
    const ticket = await getRepository(Ticket)
      .createQueryBuilder('ticket')
      .select(['ticket', 
        'seller', 
        'happening',
      ])
      .where({id: id})
      .innerJoin('ticket.seller', 'seller')
      .innerJoin('ticket.happening', 'happening')
      .getOne()

    if(!ticket || !ticket.id) throw new NotFoundError
    const riskScore = await getRiskScore(ticket)

    return {
      ...ticket,
      riskScore
    }
  }

  @Get('/happenings/:id/tickets')
  async getTicketsById(
    @Param('id') id: number,
    @QueryParams() params :SortParams
  ) {
    //defaultValues
    params.start = params.start || 0,
    params.sort = params.sort || 'post_date'
    params.dir = params.dir || "ASC"

    const tickets = await getRepository(Ticket)
      .createQueryBuilder('ticket')
      .where({"happening": id, "status": 'available' })
      .innerJoinAndSelect('ticket.seller', 'seller')
      .innerJoinAndSelect('ticket.happening', 'happening')
      .getManyAndCount()

      const ticketsWitkRiskScore = await Promise.all(tickets[0].map(async ticket => {
        const riskScore = await Promise.resolve(getRiskScore(ticket))
        return {
          riskScore: await riskScore,
          ...ticket
        }
      }))

    return {
      tickets: await ticketsWitkRiskScore,
      hasMore: tickets[1] > ((+params.start) + tickets[0].length)
    }
  }

  @Authorized()
  @Post('/tickets')
  async addTicket(
    @HttpCode(201)
    @CurrentUser() user: User,
    @Body() ticket: Partial<Ticket>
  ) {

    if(!ticket.happening) throw new BadRequestError
    if(!ticket.price) throw new BadRequestError
    if(!ticket.description) throw new BadRequestError
    if(!user || !user.id || user === null) throw new UnauthorizedError

    const finalTicket = new Ticket()
    finalTicket.postDate = new Date()
    finalTicket.seller  = user
    return Ticket.merge(finalTicket, ticket).save()
  }

  @Authorized()
  @Patch('/tickets/:id')
  //Used for editing tickets.
  async editTicket(
    @Param('id') ticketId: number,
    @CurrentUser() user: User,
    @Body() update: TicketChange
  ) {
    const ticket = await Ticket.findOne(ticketId)

    if(!ticket || !user.id) throw new NotFoundError
    if(ticket.seller.id !== user.id) throw new UnauthorizedError("This is not your ticket!")
    
    return Ticket.update(ticket, update)
      .then(_ => {
        return {
          ...ticket,
          ...update
        }
      })  
    } 

  @Authorized()
  @Patch('/tickets/:id/buy')
  //Used for buying tickets.
  async buyTicket(
    @Param('id') ticketId: number,
    @CurrentUser() user: User,
    @Body() update: TicketChange
  ) {
    const ticket = await Ticket.findOne(ticketId)
    if(!ticket) throw new NotFoundError("Ticket not found")
    if(!user) throw new UnauthorizedError
    if(ticket.seller.id === user.id) throw new UnauthorizedError("You cannot buy your own ticket.")
    
    return Ticket.update(ticket, update)
      .then(_ => {
        return {
          ...ticket,
          ...update
        }
      })
  } 

  @Authorized()
  @Delete('/tickets/:id')
  async deleteTicket(
    @Param('id') id: number,
    @CurrentUser() user: User
  ) {
    const ticket = await Ticket.findOne(id)
    if(!ticket) throw new NotFoundError("Ticket not found")
    if(ticket.seller.id !== user.id) throw new UnauthorizedError("This is not your ticket!")
    
    return Ticket.delete(ticket)
  }
}