import { 
  JsonController, 
  Get, 
  Post, 
  HttpCode, 
  Body, 
  Param, 
  Authorized, 
  Delete,
  NotFoundError,
  Put,
  UnauthorizedError,
  CurrentUser
} from 'routing-controllers'
//import User from '../users/entity'
import Comment from './entity'
import User from '../users/entity';

@JsonController()
export default class TicketsController {
  @Get('/tickets/:id/comments')
  async getComments(
    @Param('id') id : number
  ) {
    const comments = await Comment.find({where: {ticket: id}})
    console.log("COMMENTS", comments)
    return {comments: [...comments]}
  }

  @Authorized()
  @Post('/tickets/:id/comments')
  async addComment(
    @HttpCode(201)
    @CurrentUser() user: User,
    @Param('id') id: number,
    @Body() comment: Partial<Comment>
  ) {
    console.log("COMMENT --", comment)
    if(!user || !user.id) throw new UnauthorizedError
    const finalComment = new Comment()
    finalComment.user = user.id
    finalComment.ticket = id
    finalComment.postDate = new Date()

    return Comment.merge(finalComment, comment).save()
  }

  @Authorized()
  @Put('/comments/:id')
  async editComment(
    @HttpCode(201)
    @Param('id') id: number,
    @CurrentUser() user: User,
    @Body() update: Partial<Comment>
  ) {
    const comment = await Comment.findOne(id)
    if(!comment) throw new NotFoundError
    if(!user || !user.id || user.id !== comment.user) throw new UnauthorizedError

    return Comment.merge(comment, update).save()
  }

  @Authorized()
  @Delete('/comments/:id')
  async deleteComment(
    @CurrentUser() user: User,
    @Param('id') id: number,
  ) {
    const comment = await Comment.findOne(id)
    if(!comment) throw new NotFoundError
    if(!user || !user.id || user.id !== comment.user) throw new UnauthorizedError

    return Comment.delete(comment)
  }
}