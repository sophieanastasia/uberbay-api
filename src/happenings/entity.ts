import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import { MinLength, IsString, IsDate } from 'class-validator';
import Ticket from '../tickets/entity'

@Entity()
export default class Happening extends BaseEntity {
  //Use "Happening" instead of Event, because "event" is a reserved keyword
    @PrimaryGeneratedColumn()
    id?: number

    @IsString()
    @MinLength(2)
    @Column('text')
    name: string

    @IsString()
    @Column('text')
    description: string

    @IsString()
    @Column('text', {nullable: true})
    image: string

    @IsString()
    @Column('text', {nullable: true})
    location?: string

    @IsDate()
    @Column('timestamptz', {nullable: true})
    startDate: Date

    @IsDate()
    @Column('timestamptz', {nullable: true})
    endDate?: Date

    @OneToMany(_ => Ticket, ticket => ticket.happening, {nullable: true, eager: true})
    tickets: number[]

    @Column('varchar', { length: 32, nullable: true })
    timeZone: string
}