import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm'
import { IsString, IsDate } from 'class-validator'
import User from '../users/entity'
import Ticket from '../tickets/entity'

@Entity()
export default class Comment extends BaseEntity {
    @PrimaryGeneratedColumn()
    id?: number

    @IsString()
    @Column('text')
    comment: string

    @ManyToOne(_ => User, user => user.id, {eager: true}) 
    user: number

    @ManyToOne(_ => Ticket, ticket => ticket.id) 
    ticket: number

    @IsDate()
    @Column('timestamptz', {default: new Date()})
    postDate: Date

    @Column('varchar', { length: 32 })
    postTimeZone: string
}