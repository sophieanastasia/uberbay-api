import * as moment from 'moment-timezone'
import Ticket from './entity'
import Comment from '../comments/entity'
import { getRepository } from 'typeorm';
import { BadRequestError } from '../../node_modules/routing-controllers';

export const getRiskScore = async (ticket: Ticket): Promise<number> => {
  let riskScore: number
  if(!ticket || !ticket.happening || !ticket.happening.id || !ticket.id || !ticket.seller.id) throw BadRequestError
  riskScore = await getSellerRisk(ticket.seller.id) +
  await getPriceDeviation(ticket.price, ticket.happening.id) + 
  await getCommentsRisk(ticket.id) + 
  getPostTimeRisk(ticket.postDate, ticket.postTimeZone)

  return Math.min(Math.max(5, Math.round(riskScore)), 95)
}

const getSellerRisk = async (seller: number): Promise<number> => {
  const ticketCount = await Ticket.count({where: {seller: seller}})
  return ticketCount < 2 ? 5 : 0
}

const getPriceDeviation = async (ticketPrice: number, happeningId: number): Promise<number> => {
  let { avg } = await getAveragePrice(happeningId)

  if (avg === 0 || avg === ticketPrice) {
    return 0
  }
  const deviation = ((avg - ticketPrice) / avg) * 100
  return deviation < 0 ? Math.max(deviation, -10) : deviation
} 

const getCommentsRisk = async (ticketId: number): Promise<number> => {
  const count = await getCommentsCount(ticketId)
  return count <= 3 ? 0 : 5
}

const getPostTimeRisk = (time: Date, timeZone: string) => {
  const localTime = moment(time).tz(timeZone)
  return localTime.hours() < 9 || localTime.hours() >= 17 ? 10 : -10
}

//Helper functions
const getCommentsCount = async (ticketId: number) => {
  return await getRepository(Comment)
  .createQueryBuilder('ticket')
  .where('ticket_id = :ticketId', { ticketId })
  .getCount()
}

const getAveragePrice = async(happeningId: number) => {
  return await getRepository(Ticket)
    .createQueryBuilder('ticket')
    .where('happening_id = :happeningId', { happeningId })
    .select('AVG(price)', 'avg')
    .getRawOne()
}