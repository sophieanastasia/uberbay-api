import { BaseEntity, Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm'
import { IsString, IsDate } from 'class-validator';
import User from '../users/entity';
import Happening from '../happenings/entity'
import Comment from '../comments/entity'

@Entity()
export default class Ticket extends BaseEntity {
    @PrimaryGeneratedColumn()
    id?: number

    @IsString()
    @Column('text', { nullable: true })
    image: string

    @Column('numeric', { nullable: false, precision: 8, scale: 2 })
    price: number

    @IsString()
    @Column('text')
    description: string

    @ManyToOne(_ => Happening, happening => happening.id) 
    happening: Happening

    @ManyToOne(_ => User, user => user.id, {eager: true}) 
    seller: User

    @OneToMany(_ => Comment, comment => comment.ticket, {nullable: true}) 
    comments: number[]

    @IsDate()
    @Column('timestamptz', {default: new Date()})
    postDate: Date

    @Column('varchar', { length: 32 })
    postTimeZone: string

    @IsString()
    @Column('text', { default: 'available' })
    status: string
}